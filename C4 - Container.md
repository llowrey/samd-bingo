# C1 - Context

### Business with Medical Device and non Medical Device software products and services

```mermaid
C4Context
  title System Context diagram for Internet Banking System
  Person_Ext(patient, "Patient", "Receives services from Physician")
  Person_Ext(physician, "Physician", "Provides vision care")
  Enterprise_Boundary(b0, "Med Device Company") {
    System_Boundary(nonDevice, "Non-Device Services") {
      System(s0, "System0")
    }
    System_Boundary(simulationDevice, "Simulation Device") {
      System(s1, "System1")
    }
  }
```