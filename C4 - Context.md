# C4 - Context

### Business with Medical Device and non Medical Device software products and services

```mermaid
C4Context
  title System Context diagram for Internet Banking System

  Boundary(clients, "Users of the system") {
    Person_Ext(patient, "Patient", "Receives services from Physician")
    Person_Ext(physician, "Physician", "Provides vision care")
  }

  Enterprise_Boundary(ent, "Medical Device Business") {
    System(client, "Web and Mobile Apps", "Provides physical and virtual<br/> products and services")
    System(cloud, "Cloud Services")
  }

  Boundary(partners, "Data Partners", $link="https://google.com") {
    System(practice, "Medical Practice", $link="https://google.com")
    System(broker, "Medical Record Broker")
  }

  BiRel(patient, physician, "Receives Services From")
  Rel(patient, client, "Uses Services")
  Rel(physician, client, "Uses Services")
  Rel(client, cloud, "Rest / GraphQL")

  UpdateLayoutConfig($c4ShapeInRow="2", $c4BoundaryInRow="1")
```